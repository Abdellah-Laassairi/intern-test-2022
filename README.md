# Flower Classification App : Enlaps Technical Test


This technical test has been really educational. It was my first time tackling Docker, and I've learned a lot about its usage and different applications. I also had some minor issues at first with building the containers and it took more time than I anticipated (Computer storage and latency problems), however I managed to fix it after reading the Documentation (and freeing some space from my computer). Overall, I loved the experience and I'm quite eager to tackle more interesting problems and learn about more technologies in the future with the R&D team of Enlaps!


## Tasks for the technical test 

Current Progress :
- [X] develop a command line interface (CLI) program in python
- [X] which should be able to read all images in a folder (given as parameter to the CLI)
- [X] perform image classification on each image using the provided model
- [X] print the results in the console
- [X] package the app in a docker image

Bonus Tasks :
- [ ] Allow images to be loaded from an URL
- [ ] Allow images to be specified using a regexp (inference only on `images/*/img_???.jpg`)
- [ ] Train and submit your own model (we get to 75% validation accuracy without overfitting, using a 20% split for validation)
- [ ] Allow to load an external model, maybe other model formats ?



## Code Organization


### Top-level project directory layout

    .
    ├── venv                    # I used a virtual environment to create an isolated Python project.
    ├── app                     # Contains the main.py, classify.py and image_classifier.py
    │    ├── main.py
    │    ├── image_classifier.py
    │    └── classify.py 
    │                       
    ├── data                    # contains the flower labels stored in labels.txt file and the Tensorflow lite model as model.tflite.
    │    ├── labels.txt
    │    └── model.tflite         
    ├── test_images             # Contains test images extracted from the web and the training data set.
    ├── Dockerfile              # Docker container build file
    ├── requirements.txt        # Contains the necessary libraries for the Docker container build file
    └── README.md



# CLI

## For the **CLI Utilization**

   
To perform the neural network inference on all images in the `input_folder/` and print the result use :

```bash
python main.py input_folder/
```

The rest of the functionalities aren't finished yet.

&nbsp;

To perform the neural network inference on all images in a given `https://url.example/` and print the result use :

```bash
python main.py --link 'https://url.example/' -l

```

&nbsp;

To perform the neural network inference on all images using Regexp and print the result use :

```bash
python main.py --regexp 'https://url.example/' -r 

```
&nbsp;

To specify your own model use  :

```bash
python main.py --model 'path' --link 'https://url.example/'

```






