"""This is the main entrypoint of this project.
    When executed as
    python main.py input_folder/
    it should perform the neural network inference on all images in the `input_folder/` and print the results.
"""

from classify import *
from argparse import ArgumentParser
import sys


def parse_args():
    """Defining CLI arguments and returning them.

    Returns:
        Namespace: parser arguments
    """
    #Creating the parser
    parser = ArgumentParser(description="CLI Application for flower images classification.")

    #Creating a mutually exclusive group to specify data extraction type : Folder, URL, Regexp
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-i',action='store_true', help="A folder with images to analyze.")
    group.add_argument('-l',action='store_true', help="URL for the images to analyze.")
    group.add_argument('-r',action='store_true', help="Specify images to analyze using a regexp.")

    #Adding arguments to the parser
    parser.add_argument('input' , help="A folder with images to analyze.")
    parser.add_argument('--link'  ,metavar='', type=str, help="URL for the images to analyze.")
    parser.add_argument('--regexp',metavar='', type=str, help="Specify images to analyze using a regexp.")
    parser.add_argument('--model' ,metavar='', type=str, help="Load personal model.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    cli_args = parse_args()
    
    if cli_args.i or (not cli_args.l and not cli_args.r):
        # Classifying images from a folder
        print("Analyzing folder {}".format(cli_args.input), file=sys.stderr)  # info print must be done on stderr like this for messages
        classify_images(cli_args.input)
    elif cli_args.l:
        # Classifying images from a URL
        print("Analyzing URL {}".format(cli_args.link), file=sys.stderr)  # info print must be done on stderr like this for messages
        #TO DO
    elif cli_args.r:
        print("Analyzing for Regexp of {}".format(cli_args.regexp), file=sys.stderr)  # info print must be done on stderr like this for messages
        # Classifying images from a URL
        #TO DO


    