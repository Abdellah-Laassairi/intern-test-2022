from image_classifier import *
import os
import sys
import glob


def classify_images(path):

    #Loading the model.
    data_folder = "data/"
    model_path = data_folder + "model.tflite"
    label_path = data_folder + "labels.txt"
    interpreter = Interpreter(model_path)
    #print("Model Loaded Successfully.",  file=sys.stderr)

    #Allocating memory
    interpreter.allocate_tensors()
    _, height, width, _ = interpreter.get_input_details()[0]['shape']
    #print("Input tensor size: (", width, ",", height, ")", file=sys.stderr)

    # Loading images to be classified
    images = [f for f in os.listdir(path) if os.path.splitext(f)[-1] == '.jpg']

    for i in images:
        #Opening the image and converting it to RGB
        image = Image.open(path+i).convert('RGB')
        #print("Original image size {}".format (image.size), file=sys.stderr)
        
        #Resizing the image
        image = image.resize((width, height))
        #print("New image size {}".format(image.size),  file=sys.stderr)

        # Classify the image.
        label_id, prob = classify_image(interpreter, image)

        # Read class labels.
        with open(label_path, 'r') as f:
            labels = [line.strip() for i, line in enumerate(f.readlines())]

        # Return the classification label of the image.
        classification_label = labels[label_id]

        # Prints the confidence score with the most likely class
        print("{\"",i,"\": {\"score\":",'{:03.2f}'.format(prob)," \"Class\":\"",classification_label,"\"}}")
