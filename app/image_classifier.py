from tflite_runtime.interpreter import Interpreter 
from PIL import Image
import numpy as np


#Softmax function
def softmax(z):
    assert len(z.shape) == 2
    s = np.max(z, axis=1)
    s = s[:, np.newaxis] 
    e_x = np.exp(z - s)
    div = np.sum(e_x, axis=1)
    div = div[:, np.newaxis] 
    return e_x / div


def set_input_tensor(interpreter, image):  
  tensor_index = interpreter.get_input_details()[0]['index']
  # Return the input tensor based on its index.
  input_tensor = interpreter.tensor(tensor_index)()[0]
  # Assigning the image to the input tensor.
  input_tensor[:, :] = image



def classify_image(interpreter, image):
  set_input_tensor(interpreter, image)
  # Call the invoke() method from inside a function to avoid RuntimeError
  interpreter.invoke()
  output_details = interpreter.get_output_details()
  output_data = interpreter.get_tensor(output_details[0]['index'])
  #Normalize confidence score between [0 1]
  probability = softmax(output_data)
  confidence = np.max(np.unique(probability))
  return np.argmax(output_data), confidence

